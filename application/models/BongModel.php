<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BongModel extends CI_Model
{
    private $table = 'bongs';


    public $bong_code;
    public $merk;
    public $price;
    public $name;
    public $bong_type;
    public $link_image;
    public $rule = [ 
        [
            'field' => 'bong_code',
            'label' => 'bong_code',
            'rules' => 'required'
        ],
        [
            'field' => 'name',
            'label' => 'name',
            'rules' => 'required'
        ],
        [
            'field' => 'merk',
            'label' => 'merk',
            'rules' => 'required'
        ],
        [
            'field' => 'bong_type',
            'label' => 'bong_type',
            'rules' => 'required'
        ],
        [
            'field' => 'price',
            'label' => 'price',
            'rules' => 'required'
        ]
    ];
    public function Rules() { return $this->rule; }
   

    public function getAll() { return 
        $this->db->get('data_bong')->result(); 
    } 
    public function store($request) { 
        $this->bong_code = $request->bong_code; 
        $this->name = $request->name; 
        $this->merk = $request->merk;
        $this->bong_type = $request->bong_type;
        $this->price= $request->price;
        $this->link_image= $request->link_image;
        if($this->db->insert($this->table, $this)){
            return ['msg'=>'Berhasil Simpan Bong','error'=>false];
        }
        return ['msg'=>'Gagal Simpan Bong','error'=>true];
    }
    public function update($request,$id) { 
        $updateData = ['merk' => $request->merk,'bong_code' => $request->bong_code, 'name' =>$request->name, 'bong_type'=>$request->bong_type,'price'=>$request->price, 'link_image'=>$request->link_image ];
        if($this->db->where('id',$id)->update($this->table, $updateData)){
            return ['msg'=>'Berhasil Update Bong','error'=>false];
        }
        return ['msg'=>'Gagal Update Bong','error'=>true];
    }
    public function destroy($id){
        if (empty($this->db->select('*')->where(array('id' => $id))->get($this->table)->row())) return ['msg'=>'Id tidak ditemukan','error'=>true];
        
        if($this->db->delete($this->table, array('id' => $id))){
            return ['msg'=>'Berhasil Delete Bong','error'=>false];
        }
        return ['msg'=>'Gagal Delete Bong','error'=>true];
    }
}
?>