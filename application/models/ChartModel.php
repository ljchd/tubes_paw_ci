<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChartModel extends CI_Model
{
    private $table = 'charts';


    public $drug_code;
    public $merk;
    public $price;
    public $name;
    public $drug_type;
    public $link_image;
    public $rule = [ 
        [
            'field' => 'drug_code',
            'label' => 'drug_code',
            'rules' => 'required'
        ],
        [
            'field' => 'name',
            'label' => 'name',
            'rules' => 'required'
        ],
        [
            'field' => 'merk',
            'label' => 'merk',
            'rules' => 'required'
        ],
        [
            'field' => 'drug_type',
            'label' => 'drug_type',
            'rules' => 'required'
        ],
        [
            'field' => 'price',
            'label' => 'price',
            'rules' => 'required'
        ],
        [
            'field' => 'link_image',
            'label' => 'link_image',
            'rules' => 'required'
        ]
    ];
    public function Rules() { return $this->rule; }
   

    public function getAll() { return 
        $this->db->get('data_chart')->result(); 
    } 
    public function store($request) { 
        $this->drug_code = $request->drug_code; 
        $this->name = $request->name; 
        $this->merk = $request->merk;
        $this->drug_type = $request->drug_type;
        $this->price= $request->price;
        $this->link_image= $request->link_image;
        if($this->db->insert($this->table, $this)){
            return ['msg'=>'Berhasil Order','error'=>false];
        }
        return ['msg'=>'Gagal Order','error'=>true];
    }
    public function update($request,$id) { 
        $updateData = ['merk' => $request->merk, 'name' =>$request->name, 'drug_type'=>$request->drug_type,'price'=>$request->price,'link_image'=>$request->link_image ];
        if($this->db->where('id',$id)->update($this->table, $updateData)){
            return ['msg'=>'Berhasil','error'=>false];
        }
        return ['msg'=>'Gagal','error'=>true];
    }
    public function destroy($id){
        if (empty($this->db->select('*')->where(array('id' => $id))->get($this->table)->row())) return ['msg'=>'Id tidak ditemukan','error'=>true];
        
        if($this->db->delete($this->table, array('id' => $id))){
            return ['msg'=>'Berhasil Delete Chart','error'=>false];
        }
        return ['msg'=>'Gagal Delete Chart','error'=>true];
    }
    
}
?>