<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model
{
    private$table='users';
    
    
    public$id;
    public $name;
    public $password;
    public $email;
    public $user_type;
    public $created_at;
    public $link_image;
    public $verified;
    public $rule=[

        [
            'field'=>'email',
            'label'=>'email',
            'rules'=>'required|is_unique[user.email]|valid_email'
        ],
       
    ];
    public function Rules() { return$this->rule; }
    
    public function getAll() { return
        $this->db->get($this->table)->result();
    }
    public function store($request) {
        $this->name=$request->name;
        $this->email=$request->email;
        $this->password=password_hash($request->password,PASSWORD_BCRYPT);
        $this->created_at = $request->created_at;
        $this->user_type = $request->user_type;
        $this->link_image = 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQZheYQGXrxrzKGG-iVf1KHxPqepfFJbHiUHETcHdoDKa2GTXCr';
		$this->verified = 0;
        if($this->db->insert($this->table,$this)){ 
            return['msg'=>'Berhasil Simpan User','error'=>false];
        }
        return['msg'=>'Gagal Simpan User','error'=>true];
    }
    public function update($request,$id) {
        $updateData=['email'=>$request->email,'name'=>$request->name,'password'=>password_hash($request->password,PASSWORD_BCRYPT),'link_image'=>$request->link_image ,'user_type'=>$request->user_type];
        if($this->db->where('id',$id)->update($this->table,$updateData)){ 
            return['msg'=>'Berhasil Update User','error'=>false];
        }
        return['msg'=>'Gagal Update User','error'=>true];
    }
    public function destroy($id) { 
        if(empty($this->db->select('*')->where(array('id'=>$id))->get($this->table)->row()))
        return['msg'=>'Id tidak ditemukan','error'=>true];
        
        if($this->db->delete($this->table,array('id'=>$id))){
            return['msg'=>'Berhasil Delete User','error'=>false];
            }
            return['msg'=>'Gagal Delete User','error'=>true];
        }
        public function verify($request){
            $user = $this->db->select('*')->where(array('email' => $request->email))->get($this->table)->row_array();
            if(!empty($user) && password_verify($request->password , $user['password'])) {
                return $user;
            } else {
                return false;
            }
        }
		public function verifyemail($email){
			$where=['email'=>$email];
			$update=['verified'=>1];
			$this->db->where($where)->update($this->table,$update);
			return['msg'=>'Congratulations, your email has been verified. Please log in again to get high with us!','error'=>false];
		}
    }
?>