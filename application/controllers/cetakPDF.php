<?php
Class cetakPDF extends CI_Controller{
    
        public  function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST, DELETE");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding, Authorization");
        parent::__construct();
        $this->load->library('convertPDF');
    }
    
    function index(){
        $pdf = new FPDF('P','mm','A4');
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',16); 
        $pdf->Cell(190,7,'PESANAN THE HIGH FIVE ',0,1,'C');
        $pdf->SetFont('Arial','B',12);

        $pdf->Cell(8,7,'',0,1);

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(30,6,'Kode',1,0);
        $pdf->Cell(50,6,'Nama Obat',1,0);
        $pdf->Cell(50,6,'Merek',1,0);
        $pdf->Cell(25,6,'Tipe',1,0);
        $pdf->Cell(30,6,'Harga',1,1);

        $jadwal = $this->db->get('charts')->result();
        foreach ($jadwal as $row){
            $pdf->Cell(30,6,$row->drug_code,1,0);
            $pdf->Cell(50,6,$row->name,1,0);
            $pdf->Cell(50,6,$row->merk,1,0);
            $pdf->Cell(25,6,$row->drug_type,1,0);
            $pdf->Cell(30,6,$row->price,1,1);   
        }
        $pdf->Cell(8,7,'',0,1);
        $pdf->Cell(8,7,'',0,1);
        $pdf->Cell(190,7,'Diharapkan membawa struk ini untuk transaksi pembayaran di THE HIGH FIVE',0,1,'C');
        $pdf->Output();
    }
}