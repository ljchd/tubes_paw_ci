<?php
use Restserver \Libraries\REST_Controller ;
Class Bong extends REST_Controller{
    public function __construct(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST, DELETE");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding, Authorization");
        parent::__construct();
        $this->load->model('BongModel');
        $this->load->library('form_validation');
    }
    public function index_get(){
        return $this->returnData($this->db->get('bongs')->result(), false);
    }
    public function index_post($id = null){
        $validation = $this->form_validation;
        $rule = $this->BongModel->rules();
        if($id == null){
            array_push($rule,[
                    'field' => 'bong_code',
                    'label' => 'bong_code',
                    'rules' => 'required|is_unique[bongs.bong_code]'
                ],
                [
                    'field' => 'name',
                    'label' => 'name',
                    'rules' => 'required'
                ],
                [
                    'field' => 'merk',
                    'label' => 'merk',
                    'rules' => 'required'
                ],
                [
                    'field' => 'bong_type',
                    'label' => 'bong_type',
                    'rules' => 'required'
                ],
                [
                    'field' => 'price',
                    'label' => 'price',
                    'rules' => 'required'
                ]
            );
        }
        else{
            array_push($rule,
                [
                    'field' => 'bong_code',
                    'label' => 'bong_code',
                    'rules' => 'required'
                ]
            );
        }
        $validation->set_rules($rule);
		if (!$validation->run()) {
			return $this->returnData($this->form_validation->error_array(), true);
        }
        $bong = new BongData();
        $bong->name = $this->post('name');
        $bong->bong_code = $this->post('bong_code');
        $bong->merk = $this->post('merk');
        $bong->bong_type = $this->post('bong_type');
        $bong->price = $this->post('price');
        $bong->link_image = $this->post('link_image');
        if($id == null){
            $response = $this->BongModel->store($bong);
        }else{
            $response = $this->BongModel->update($bong,$id);
        }
        return $this->returnData($response['msg'], $response['error']);
    }
    public function index_delete($id = null){
        if($id == null){
			return $this->returnData('Parameter Id Tidak Ditemukan', true);
        }
        $response = $this->BongModel->destroy($id);
        return $this->returnData($response['msg'], $response['error']);
    }
    public function returnData($msg,$error){
        $response['error']=$error;
        $response['message']=$msg;
        return $this->response($response);
    }
}
Class BongData{
    public $bong_code;
    public $merk;
    public $price;
    public $name;
    public $bong_type;
    public $link_image;

}