<?php

use Restserver \Libraries\REST_Controller;

Class User extends REST_Controller {
    public function __construct() { 
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Methods:GET,OPTIONS,POST,DELETE");
        header("Access-Control-Allow-Headers:Content-Type,Content-Length,Accept-Encoding,Authorization");

        parent::__construct();
        $this->load->model('UserModel');
        $this->load->library('form_validation');
        $this->load->library('email', "kontol");
        $this->load->helper(['jwt', 'authorization']);
    }
    public function index_get() {
        return$this->returnData($this->db->get('users')->result(),false);
    }
    public function index_post($id=null) {
        $validation=$this->form_validation;
        $rule=$this->UserModel->rules();
        if($id==null) {
                
            array_push($rule,
            [
                'field'=>'password',
                'label'=>'password',
                'rules'=>'required'
            ],
            [
                'field'=>'email',
                'label'=>'email',
                'rules'=>'required|valid_email|is_unique[users.email]'
            ]
        );
    }
        else {
            array_push($rule,
            [
                'field'=>'email',
                'label'=>'email',
                'rules'=>'required|valid_email'
            ]
        );
    }
    $validation->set_rules($rule);
    if(!$validation->run()) {
        return$this->returnData($this->form_validation->error_array(),true);
    }
    date_default_timezone_set('Asia/Jakarta');
    $now = date('Y-m-d H:i:s');
    $user=new UserData();
    $user->name=$this->post('name');
    $user->password=$this->post('password');
    $user->email=$this->post('email');
    $user->created_at= $now;
    $user->user_type = $this->post('user_type');
    $user->link_image = $this->post('link_image');
	$user->verified = $this->post('verified');
	$user_email = $user->email;
    
    if($id==null) {
        $response=$this->UserModel->store($user);
        $link = 'http://serve.drugsative.xyz/index.php/user/verifyemail?email='.$user_email;
        
        $config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'mail.drugsative.xyz',
            'smtp_user' => 'drugsati_drugsative',  // Email gmail
            'smtp_pass'   => 'kTl4M0bWn;%l',  // Password gmail
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465,
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        ];

        // Load library email dan konfigurasinya
        $this->load->library('email', $config);

        // Email dan nama pengirim
        $this->email->from('admin@drugsative.xyz', 'Drugsative - Admin');

        // Email penerima
        $this->email->to($user_email); // Ganti dengan email tujuan

        // Subject email
        $this->email->subject('Please Verify your Email');

        // Isi email
        $this->email->message('Thank you for joining us! Please click the link below to complete your user registration
           <a href= "'.$link.'">Click here to verify</a>With Love,Admin');

        // Tampilkan pesan sukses atau error
        $this->email->send();
        
    }else{
        $response=$this->UserModel->update($user,$id);
    }
    return$this->returnData($response['msg'],$response['error']);
}
    public function index_delete($id=null) {
        if($id==null){ 
            return$this->returnData('Parameter Id Tidak Ditemukan',true);
        }
        $response=$this->UserModel->destroy($id);
        return$this->returnData($response['msg'],$response['error']);
    }
    public function returnData($msg,$error) {
        $response['error']=$error;
        $response['message']=$msg;
        return$this->response($response);
    }
	public function verifyemail_get(){
        $email=$this->get('email');
        $response=$this->UserModel->verifyemail($email);
        return$this->returnData($response['msg'],$response['error']);
    }
}
Class UserData {
    public $name;
    public $password;
    public $email;
    public $user_type;
    public $created_at;
    public $link_image;
    public $verified;
}