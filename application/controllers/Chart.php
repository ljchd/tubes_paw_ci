<?php
use Restserver \Libraries\REST_Controller ;
Class Chart extends REST_Controller{
    public function __construct(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST, DELETE");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
        parent::__construct();
        $this->load->model('ChartModel');
        $this->load->library('form_validation');
    }
    public function index_get(){
        return $this->returnData($this->db->get('charts')->result(), false);
    }
    public function index_post($id = null){
        $validation = $this->form_validation;
        $rule = $this->ChartModel->rules();
        if($id == null){
            array_push($rule,[
                    'field' => 'drug_code',
                    'label' => 'drug_code',
                    'rules' => 'required'
                ],
                [
                    'field' => 'name',
                    'label' => 'name',
                    'rules' => 'required'
                ],
                [
                    'field' => 'merk',
                    'label' => 'merk',
                    'rules' => 'required'
                ],
                [
                    'field' => 'drug_type',
                    'label' => 'drug_type',
                    'rules' => 'required'
                ],
                [
                    'field' => 'price',
                    'label' => 'price',
                    'rules' => 'required'
                ],
                [
                    'field' => 'link_image',
                    'label' => 'link_image',
                    'rules' => 'required'
                ]
            );
        }
        else{
            array_push($rule,
                [
                    'field' => 'drug_code',
                    'label' => 'drug_code',
                    'rules' => 'required|is_unique[drugs.drug_code]'
                ]
            );
        }
        $validation->set_rules($rule);
		if (!$validation->run()) {
			return $this->returnData($this->form_validation->error_array(), true);
        }
        $chart = new ChartData();
        $chart->name = $this->post('name');
        $chart->drug_code = $this->post('drug_code');
        $chart->merk = $this->post('merk');
        $chart->drug_type = $this->post('drug_type');
        $chart->price = $this->post('price');
        $chart->link_image = $this->post('link_image');
        if($id == null){
            $response = $this->ChartModel->store($chart);
        }else{
            $response = $this->ChartModel->update($chart,$id);
        }
        return $this->returnData($response['msg'], $response['error']);
    }
    public function index_delete($id = null){
        if($id == null){
			return $this->returnData('Parameter Id Tidak Ditemukan', true);
        }else if($id == -1)
        {
            $hapus = $this->db->get('charts')->result();
        foreach ($hapus as $row){
            $response = $this->ChartModel->destroy($row->id);
        }
        }
        $response = $this->ChartModel->destroy($id);
        return $this->returnData($response['msg'], $response['error']);
    }
    public function returnData($msg,$error){
        $response['error']=$error;
        $response['message']=$msg;
        return $this->response($response);
    }
}
Class ChartData{
    public $drug_code;
    public $merk;
    public $price;
    public $name;
    public $drug_type;
    public $link_image;

}