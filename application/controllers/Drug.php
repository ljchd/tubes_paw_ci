<?php
use Restserver \Libraries\REST_Controller ;
Class Drug extends REST_Controller{
    public function __construct(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST, DELETE");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding, Authorization");
        parent::__construct();
        $this->load->model('DrugModel');
        $this->load->library('form_validation');
    }
    public function index_get(){
        return $this->returnData($this->db->get('drugs')->result(), false);
    }
    public function index_post($id = null){
        $validation = $this->form_validation;
        $rule = $this->DrugModel->rules();
        if($id == null){
            array_push($rule,[
                    'field' => 'drug_code',
                    'label' => 'drug_code',
                    'rules' => 'required|is_unique[drugs.drug_code]'
                ],
                [
                    'field' => 'name',
                    'label' => 'name',
                    'rules' => 'required'
                ],
                [
                    'field' => 'merk',
                    'label' => 'merk',
                    'rules' => 'required'
                ],
                [
                    'field' => 'drug_type',
                    'label' => 'drug_type',
                    'rules' => 'required'
                ],
                [
                    'field' => 'price',
                    'label' => 'price',
                    'rules' => 'required'
                ],
                [
                    'field' => 'link_image',
                    'label' => 'link_image',
                    'rules' => 'required'
                ]
            );
        }
        else{
            array_push($rule,
                [
                    'field' => 'drug_code',
                    'label' => 'drug_code',
                    'rules' => 'required'
                ]
            );
        }
        $validation->set_rules($rule);
		if (!$validation->run()) {
			return $this->returnData($this->form_validation->error_array(), true);
        }
        $drug = new DrugData();
        $drug->name = $this->post('name');
        $drug->drug_code = $this->post('drug_code');
        $drug->merk = $this->post('merk');
        $drug->drug_type = $this->post('drug_type');
        $drug->price = $this->post('price');
        $drug->link_image = $this->post('link_image');
        if($id == null){
            $response = $this->DrugModel->store($drug);
        }else{
            $response = $this->DrugModel->update($drug,$id);
        }
        return $this->returnData($response['msg'], $response['error']);
    }
    public function index_delete($id = null){
        if($id == null){
			return $this->returnData('Parameter Id Tidak Ditemukan', true);
        }
        $response = $this->DrugModel->destroy($id);
        return $this->returnData($response['msg'], $response['error']);
    }
    public function returnData($msg,$error){
        $response['error']=$error;
        $response['message']=$msg;
        return $this->response($response);
    }
}
Class DrugData{
    public $drug_code;
    public $merk;
    public $price;
    public $name;
    public $drug_type;
    public $link_image;

}