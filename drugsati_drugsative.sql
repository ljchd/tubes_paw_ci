-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2019 at 12:55 AM
-- Server version: 5.7.28-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drugsati_drugsative`
--

-- --------------------------------------------------------

--
-- Table structure for table `bongs`
--

CREATE TABLE `bongs` (
  `id` int(192) NOT NULL,
  `bong_code` varchar(192) NOT NULL,
  `name` varchar(192) NOT NULL,
  `merk` varchar(192) NOT NULL,
  `bong_type` varchar(192) NOT NULL,
  `price` float NOT NULL,
  `link_image` varchar(192) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bongs`
--

INSERT INTO `bongs` (`id`, `bong_code`, `name`, `merk`, `bong_type`, `price`, `link_image`) VALUES
(2, 'BG-01', 'Syringe 5cc', 'Kalbe', 'Medium', 20000, 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR4zr-m42x0Z0h7uGvYLq4tbjvSNYuGEtbZ-FOsGCW61Ba83-KH'),
(3, 'BG-02', 'Dopezilla Glass Tokyo Beaker', 'Dopezilla', 'Medium', 650000, 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQYQX4MXoGWUpwrET9JZvO0GFi1rQmR-71DFjZfvdw1DN_oZvxY'),
(4, 'BG-03', 'Rasta Portable Acrylic', 'Rasta', 'Small', 450000, 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTjlbcWVNPIEds7yeuUJw5KkAIijsSlp7XnvaYp6rghq0Pzz64g');

-- --------------------------------------------------------

--
-- Table structure for table `charts`
--

CREATE TABLE `charts` (
  `id` int(10) NOT NULL,
  `drug_code` varchar(192) NOT NULL,
  `name` varchar(192) NOT NULL,
  `merk` varchar(192) NOT NULL,
  `drug_type` varchar(192) NOT NULL,
  `price` float NOT NULL,
  `link_image` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drugs`
--

CREATE TABLE `drugs` (
  `id` int(10) NOT NULL,
  `drug_code` varchar(192) NOT NULL,
  `name` varchar(192) NOT NULL,
  `merk` varchar(192) NOT NULL,
  `drug_type` varchar(192) NOT NULL,
  `price` float NOT NULL,
  `link_image` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drugs`
--

INSERT INTO `drugs` (`id`, `drug_code`, `name`, `merk`, `drug_type`, `price`, `link_image`) VALUES
(1, 'DG-01', 'Glidabet 80MG', 'Glidabet', 'Strong', 1000, 'https://images.k24klik.com/product/apotek_online_k24klik_201607260430231922_19-Glidabet.jpg'),
(2, 'DG-02', 'RL OTSU 500ML', 'Otsu', 'Strong', 22000, 'https://images.k24klik.com/product/apotek_online_k24klik_20160722110929291_258-Infus-Otsu-RL.jpg'),
(3, 'DG-03', 'METHOTREXAT 2,5 TAB', 'Methotrex', 'Strong', 3000, 'https://images.k24klik.com/product/apotek_online_k24klik_201607280218373754_109-Methotrexat.jpg'),
(4, 'DG-04', 'TELFAST PLUS TAB', 'Telfast', 'Strong', 8000, 'https://images.k24klik.com/product/0118a0106.jpg'),
(5, 'DG-05', 'FEROSPAT TAB', 'Ferospat', 'Soft', 90000, 'https://images.k24klik.com/product/0113f1228.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(192) NOT NULL,
  `email` varchar(192) NOT NULL,
  `password` varchar(192) NOT NULL,
  `user_type` varchar(192) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `link_image` varchar(192) NOT NULL,
  `verified` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `user_type`, `created_at`, `link_image`, `verified`) VALUES
(1, 'admin', 'admin@drugsative.xyz', '$2y$10$eHWcW.88J7KOYJUpkoYX3OoIogzB4/JJ7W53SKaEg5XZhPpwzGgCu', '1', '2019-11-25 17:53:07', 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQZheYQGXrxrzKGG-iVf1KHxPqepfFJbHiUHETcHdoDKa2GTXCr', 1),
(2, 'user', 'user@gmail.com', '$2y$10$DwEsXT0QeAp7I.bn2zlWd.ZvV0Ata6d9CmHEbatl6kuA5gh5XFvBi', '1', '2019-11-25 17:53:52', 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQZheYQGXrxrzKGG-iVf1KHxPqepfFJbHiUHETcHdoDKa2GTXCr', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bongs`
--
ALTER TABLE `bongs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bong_code` (`bong_code`);

--
-- Indexes for table `charts`
--
ALTER TABLE `charts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drugs`
--
ALTER TABLE `drugs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `drug_code` (`drug_code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bongs`
--
ALTER TABLE `bongs`
  MODIFY `id` int(192) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `charts`
--
ALTER TABLE `charts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drugs`
--
ALTER TABLE `drugs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
